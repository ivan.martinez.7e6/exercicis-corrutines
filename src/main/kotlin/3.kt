import kotlinx.coroutines.delay
import java.io.File
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis

suspend fun main(){

    val lyrics = File("src/main/kotlin/lyrics").readLines()
    var totalTime = 0L
    for (line in lyrics){
        val time = measureTimeMillis {

            delay(3000)
            println(line)
        }
        totalTime += time

    }
    val seconds = (totalTime/1000)%60
    val minutes =  (totalTime/1000)/60
    println("$minutes:$seconds")


}