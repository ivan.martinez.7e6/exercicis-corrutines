import kotlinx.coroutines.delay
import java.io.File
import java.util.*
import kotlin.system.measureTimeMillis

val scanner = Scanner(System.`in`)

suspend fun main(){
    val randomNumber = (0..50).random()
    println("You have 10 seconds to guess the secret number...")

    var totalTime = 0L
    while (totalTime < 10000L){
        val time = measureTimeMillis {
            print("Enter a number:")
            val userNumber = scanner.nextInt()
            if (userNumber != randomNumber){
                println("Wrong number, try again")
            } else{
                println("You got it!")
                totalTime = 10000L
            }
            delay(100)
        }
        totalTime += time
    }
    println("Time is up!")










}