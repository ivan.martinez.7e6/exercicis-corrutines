import kotlinx.coroutines.*

suspend fun main() {
    val horses = arrayOf(
        Horse("Eustaquio"),
        Horse("Colinabo"),
        Horse("Pepe"),
        Horse("Zanahorio"))

    runBlocking {
         horses.forEach { horse ->
           launch { horse.run() }
        }
    }
}

class Horse(val name: String){
    val randomDelays = arrayOf<Long>(1000, 2000, 3000, 4000).random()
     suspend fun run(){
        for (lap in 1..4){
            delay(randomDelays)
            when (lap){
                1 -> println("$name completes lap $lap!")
                2-> println("$name completes lap $lap!")
                3-> println("$name completes lap $lap!")
                4 -> println("$name finishes the race!")
            }
        }

    }


}
