import kotlinx.coroutines.*

suspend fun main() {
    coroutineScope {
        launch {
            corroutine1()
        }
        launch {
            corroutine2()
        }


    }
    println("Completed")
}
suspend fun corroutine1() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}
suspend fun corroutine2() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}
