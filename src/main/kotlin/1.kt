import kotlinx.coroutines.*

suspend fun main() {
    //exercici1()
    //exercici1_1()

   // exercici1_2()
    exercici1_3()
}


fun exercici1_2(){
    println("The main program is started")
    doBackground()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
fun doBackground(){
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
}

suspend fun exercici1_3(){
    println("The main program is started")
    doBackgroundWithContext()
    println("The main program continues")
    runBlocking {
        delay(1100)
        println("The main program is finished")
    }
}
suspend fun doBackgroundWithContext(){
    withContext(Dispatchers.Default) {
        launch{
            println("Background processing started")
            delay(1000)
            println("Background processing finished")
        }
    }
}
fun exercici1_1(){
    println("The main program is started")    // 1
    GlobalScope.launch {
        println("Background processing started")  // 3
        delay(1000)
        println("Background processing finished") // (*)
    }
    println("The main program continues")  // 2
    runBlocking {
        delay(900)
        println("The main program is finished") // 4
        delay(100)
    }

    //(*) El println de després del delay del launch no es mostra perquè quan fa el println del runBlocking
    // acaba el programa abans que acabi el delay.
    // Si afegim un delay després del println del runBlocking sí que es mostrarà perquè hi haurà temps
}
fun exercici1(){
    println("The main program is started")    // 1
    GlobalScope.launch {
        println("Background processing started")  // 3
        delay(1000)
        println("Background processing finished") // 4
    }

    println("The main program continues")  // 2
    runBlocking {
        delay(1500)
        println("The main program is finished") // 5
    }
}