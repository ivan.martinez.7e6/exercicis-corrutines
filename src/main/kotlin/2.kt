import kotlinx.coroutines.*

suspend fun main() {
    exercici2()
   //exercici2_1()
}



suspend fun exercici2_1(){
    withContext(Dispatchers.Default){
        launch {
            println("Hello World")
            delay(100)
            println("Message 1")
            delay(100)
        }
    }
    println("Finished!")
}
suspend fun exercici2(){
    helloPrinterV1()
    println("Finished!")
}

suspend fun helloPrinterV1(){
    withContext(Dispatchers.Default){
        launch {
            println("Hello World")
            delay(100)
            println("Message 1")
            delay(100)
        }

    }

}